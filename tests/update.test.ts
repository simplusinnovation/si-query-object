import { chirp, Update } from '../src'
import chai = require('chai')

interface UpdateTestStructure {
	test: {cmd: Update, opt: {prefix: string}}
	sql?: string
	arango?: string
	mongo?: string
}

const updates: UpdateTestStructure[] = [
	{
		test: {
			cmd: {$set: [{test1: 'test1'}, {test2: 'test2'}]},
			opt: {prefix: `u`},
		},
		sql: `u.test1 = test1 , u.test2 = test2`,
		arango: `{test1: "test1", test2: "test2"}`,
	},
	{
		test: {
			cmd: {$set: {test3: 'test3'}},
			opt: {prefix: `u`},
		},
		sql: `u.test3 = test3`,
		arango: `{test3: "test3"}`,
	},
	{
		test: {
			cmd: {$inc: [{test4: 1}, {test5: 2}]},
			opt: {prefix: `u`},
		},
		sql: `u.test4 = (u.test4 + 1) , u.test5 = (u.test5 + 2)`,
		arango: `{test4: (u.test4 + 1), test5: (u.test5 + 2)}`,
	},
	{
		test: {
			cmd: {$inc: {test6: 1}},
			opt: {prefix: `u`},
		},
		sql: `u.test6 = (u.test6 + 1)`,
		arango: `{test6: (u.test6 + 1)}`,
	},
	{
		test: {
			cmd: {$dec: [{test7: 1}, {test8: 2}]},
			opt: {prefix: `u`},
		},
		sql: `u.test7 = (u.test7 - 1) , u.test8 = (u.test8 - 2)`,
		arango: `{test7: (u.test7 - 1), test8: (u.test8 - 2)}`,
	},
	{
		test: {
			cmd: {$dec: {test9: 1}},
			opt: {prefix: `u`},
		},
		sql: `u.test9 = (u.test9 - 1)`,
		arango: `{test9: (u.test9 - 1)}`,
	},
	{
		test: {
			cmd: {profile: {name: 'Athenkosi', surname: 'Mase'}},
			opt: {prefix: `u`},
		},
		sql: `u.profile = {"name":"Athenkosi","surname":"Mase"}`,
		arango: `{profile: {"name": "Athenkosi", "surname": "Mase"}}`,
	},
	{
		test: {
			cmd: {
				benchmarks: [{
					name: 'Test Benchmark'
					, category: ['Benchmark 1', 'Benchmark 3']
				}],
				products: ['product 1', 'product 3'],
				siteMapping: [
					{name: 'SLM 1', alias: 'Test 1'}
					, {name: 'SLM 2', alias: 'Test 2'}
				]
			},
			opt: {prefix: `u`},
		},
		sql: `u.benchmarks = [{"name":"Test Benchmark","category":["Benchmark 1","Benchmark 3"]}] , u.products = ["product 1","product 3"] , u.siteMapping = [{"name":"SLM 1","alias":"Test 1"},{"name":"SLM 2","alias":"Test 2"}]`,
		arango: `{benchmarks: [{"name": "Test Benchmark", "category": ["Benchmark 1", "Benchmark 3"]}], products: ["product 1", "product 3"], siteMapping: [{"name": "SLM 1", "alias": "Test 1"}, {"name": "SLM 2", "alias": "Test 2"}]}`,
	}
]




const parseUpdate = (update: {cmd: Update, opt: {prefix: string}}, technology: string): string => {
	switch (technology) {
		case `sql`:
			return chirp.Update.SQL(update.cmd, update.opt).toString()
		case `arango`:
			return chirp.Update.Arango(update.cmd, update.opt).toAQL()
		default:
			return ``
	}
}

describe('Chirp UPDATE function', function (): void {
	describe(`Chirp update statements tests for ${String(updates.length)} tests`, function (): void {
		updates.forEach((update, index, arr) => {
			it(`should print SQL 'UPDATE SET STATEMENT' for test ${String(index + 1)}`, function (done: MochaDone): void {
				chai.expect(parseUpdate(update.test, `sql`)).to.be.a(`string`)
				chai.expect(parseUpdate(update.test, `sql`)).to.equal(update.sql)
				done()
				console.info('\x1b[33m%s\x1b[0m', '   query =>: ', parseUpdate(update.test, `sql`))
			})
			it(`should print ARANGO 'UPDATE SET STATEMENT' for test ${String(index + 1)}`, function (done: MochaDone): void {
				chai.expect(parseUpdate(update.test, `arango`)).to.be.a(`string`)
				chai.expect(parseUpdate(update.test, `arango`)).to.equal(update.arango)
				done()
				console.info('\x1b[33m%s\x1b[0m', '   query =>: ', parseUpdate(update.test, `arango`))
			})
		})
	})
})