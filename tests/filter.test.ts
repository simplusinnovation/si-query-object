import { chirp, Filter } from '../build'
import chai = require('chai')

interface FilterTestStructure {
	test: Filter
	sql?: string
	arango?: string
	mongo?: string
}
const filters: FilterTestStructure[] = [{
		test: {
			test1: {$eq: 'test1'},
			test2: {$eq: 'test2'},
		},
		sql: `u.test1 = 'test1' AND u.test2 = 'test2'`,
		arango: `(u.test1 == "test1") && (u.test2 == "test2")`,
	}, {
		test: {
			test3: {$neq: 'test3'},
		},
		sql: `u.test3 != 'test3'`,
		arango: `u.test3 != "test3"`,
	}, {
		test: {
			test4: {$lt: 1},
		},
		sql: `u.test4 < 1`,
		arango: `u.test4 < 1`,
	}, {
		test: {
			test5: { $lte: 1},
		},
		sql: `u.test5 <= 1`,
		arango: `u.test5 <= 1`,
	}, {
		test: {
			test6: {$gt: 1},
		},
		sql: `u.test6 > 1`,
		arango: `u.test6 > 1`,
	}, {
		test: {
			test7: {$gte: 1},
		},
		sql: `u.test7 >= 1`,
		arango: `u.test7 >= 1`,
	}, {
		test: {
			test8: {$in: ['word1', 'word2', 'word3']},
		},
		sql: `u.test8 IN ['word1','word2','word3']`,
		arango: `u.test8 in ["word1", "word2", "word3"]`,
	}, {
		test: {
			$and: [
					{test9: {$eq: 'test9'}},
					{test10: {$eq: 'test10'}},
				],
		},
		sql: `(u.test9 = 'test9') AND (u.test10 = 'test10')`,
		arango: `(u.test9 == "test9") && (u.test10 == "test10")`,
	}, {
		test: {
			$or: [
					{test11: {$eq: 'test11'}},
					{test12: {$eq: 'test12'}},
				],
		},
		sql: `(u.test11 = 'test11') OR (u.test12 = 'test12')`,
		arango: `(u.test11 == "test11") || (u.test12 == "test12")`,
	}, {
		test: {
			$or: [
					{test13: {$eq: 'test13'}},
					{test14: {$eq: 'test14'}},
					{$and: [
							{test15: {$eq: 'test15'}},
							{test16: {$eq: 'test16'}},
						]},
				],
		},
		sql: `(u.test13 = 'test13') OR (u.test14 = 'test14') OR ((u.test15 = 'test15') AND (u.test16 = 'test16'))`,
		arango: `((u.test13 == "test13") || (u.test14 == "test14")) || ((u.test15 == "test15") && (u.test16 == "test16"))`,
	}, {
		test: {
			$not: {
				$or: [
					{test17: {$eq: 'test17'}},
					{test18: {$eq: 'test18'}},
				],
			},
		},
		sql: `NOT ((u.test17 = 'test17') OR (u.test18 = 'test18'))`,
		arango: `!((u.test17 == "test17") || (u.test18 == "test18"))`,
	},
]

const parseFilter = (filter: Filter, technology: string): string => {
	switch (technology) {
		case 'sql':
			return chirp.Filter.SQL(filter, {prefix : 'u'}).toString()
		case 'arango':
			return chirp.Filter.Arango(filter, {prefix : 'u'}).toAQL()
		default:
			return ''
	}
}

describe('Chirp FILTER function', function (): void {
	describe(`Chirp Filter statements tests for  ${String(filters.length)} tests`, function (): void {
		filters.forEach((filter, index, arr) => {
			it(`should print SQL FILTER STATEMENT for test ${String(index + 1)}`, function (done: MochaDone): void {
				chai.expect(parseFilter(filter.test, 'sql')).to.be.a('string')
				chai.expect(parseFilter(filter.test, 'sql')).to.equal(filter.sql)
				done()
				console.info('\x1b[33m%s\x1b[0m', '   query => : ', parseFilter(filter.test, 'sql'))
			})
			it(`should print ARANGO FILTER STATEMENT for test ${String(index + 1)}`, function (done: MochaDone): void {
				chai.expect(parseFilter(filter.test, 'arango')).to.be.a('string')
				chai.expect(parseFilter(filter.test, 'arango')).to.equal(filter.arango)
				done()
				console.info('\x1b[33m%s\x1b[0m', '   query => : ', parseFilter(filter.test, 'sql'))
			})
		})
	})
})