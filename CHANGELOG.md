# Changelog
All notable changes to this library will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

[comment]: <> (######################################################)
[comment]: <> (############ Version 0.1.3 Release ###################)
[comment]: <> (######################################################)
## [0.1.3] - 2018-04-02
### NEW
#### General
- Changelog
#### Methods - Filter
- `Arango` parser for filter types with optional column prefix option
- 11 filter tests
#### Methods - Update
- `Arango` parser for update types
- 6 update tests
### FIXES
- `Arango` allows for nested objects

[comment]: <> (######################################################)
[comment]: <> (############ Version 0.0.1 Release ###################)
[comment]: <> (######################################################)
## [0.0.1] - 2018-03-16
[comment]: <> (### CHANGES - BREAKING)
### NEW
#### General
- Changelog
- TSLint
- TSConfig
#### Methods -  Filter
- Filter types with methods
  * BinaryOperations
    * equals
    * not equal
    * less than
    * less than and equal
    * greater than
    * greater than and equal
    * included in
  * LogicalOperations
    * Or
    * Not
    * And
- SQL parser for filter types
- Optional filter column prefix option
- 11 filter tests
#### Methods - Update
- Update types with methods
  * UpdateOperations
    * set
    * increment
    * decrement
- SQL parser for update types
- Non-optional update column prefix option
- 6 update tests