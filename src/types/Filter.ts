export interface BinaryOperations {
	$eq?: string | number | null
	$neq?: string | number | null
	$lt?: number | string
	$lte?: number | string
	$gt?: number | string
	$gte?: number | string
	// $exists?: string
	// $like?: string | RegExp
	$in?: string[]
}

export interface BinaryFilter {
	[field: string]: BinaryOperations
}

export interface NotRetFilter {
	$or?: OrRetFilter[]
	$and?: AndRetFilter[]
	[field: string]: BinaryOperations | OrRetFilter[]  | AndRetFilter[] | undefined
}

export interface AndRetFilter {
	$or?: OrRetFilter[]
	$not?: NotRetFilter
	[field: string]: BinaryOperations | OrRetFilter[] | NotRetFilter | undefined
}

export interface OrRetFilter {
	$and?: AndRetFilter[]
	$not?: NotRetFilter
	[field: string]: BinaryOperations | AndRetFilter[] | NotRetFilter | undefined
}

export interface Filter {
	$or?: OrRetFilter[]
	$and?: AndRetFilter[]
	$not?: NotRetFilter
	[field: string]: BinaryOperations | OrRetFilter[]  | NotRetFilter | AndRetFilter[] |undefined
}

export const keywords = ['$or' , '$and' , '$not']

export default Filter