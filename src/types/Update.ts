export interface Update {
	$set?: UpdateAny[] | UpdateAny
	$inc?: UpdateNumber[] | UpdateNumber
	$dec?: UpdateNumber[] | UpdateNumber
	// $push?: UpdateAny[] | UpdateAny
	// $pop?: UpdateAny[] | UpdateAny
	// $negate?: string[] | string
	[field: string]: UpdateAny | UpdateAny[] | UpdateNumber | UpdateNumber[] | object | string | boolean | number | undefined
}

export interface UpdateNumber {
	[field: string]: number
}
export interface UpdateAny {
	// tslint:disable-next-line:no-any
	[field: string]: any
}

export const keywords = ['$set', '$inc', '$dec']

export default Update