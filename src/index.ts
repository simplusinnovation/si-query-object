/**
 * @license
 * Copyright 2016 Progressive Accessibility Solutions, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //////////////////////////////////////////////////////////////////////////////////////////////////
// Chirp requirements
/////////////////////////////////////////////////////////////////////////////////////////////////
import { Filter, Update } from './types'
import { FilterOptions as ArangoFilterOptions, ArangoFilter, UpdateOptions as ArangoUpdateOptions, ArangoUpdate, AQLfunctions, BinaryOperation, NAryOperation, ObjectLiteral } from './facades/Arango';
import { FilterOptions as SQLFilterOptions, SQLFilter , UpdateOptions as SQLUpdateOptions , SQLUpdate } from './facades/Sql';
import { Expression } from 'squel';

//////////////////////////////////////////////////////////////////////////////////////////////////
// Chirp Interface

 /**Chirp
  *
  * chirp method object for various syntaxes
  *
  * available syntaxes:
  *        * Arango
  *         * SQL
  */
export interface Chirp {
	Filter: {
		Arango(filter: Filter, options?: ArangoFilterOptions): AQLfunctions | BinaryOperation | NAryOperation | null
		SQL(filter: Filter, options?: SQLFilterOptions): Expression | null,
	}
	Update: {
		Arango(update: Update, options?: ArangoUpdateOptions): ObjectLiteral | null
		SQL(update: Update, options: SQLUpdateOptions): {toString(): string} | null,
	}
}

 /**Chirp
  *
  * chirp method object for various syntaxes
  *
  * available syntaxes:
  *        * Arango
  *         * SQL
  */
export const chirp: Chirp = {
	Filter: {
		SQL : SQLFilter,
		Arango : ArangoFilter,
	},
	Update: {
		SQL: SQLUpdate,
		Arango : ArangoUpdate,
	},
}

export {
	Filter,
	Update,
}

export default chirp