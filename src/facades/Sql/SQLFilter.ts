//////////////////////////////////////////////////////////////////////////////////////////////////
// SQLFilter requirements
/////////////////////////////////////////////////////////////////////////////////////////////////
import { Filter, FilterKeywords as keywords } from '../../types'
import { addTableAlias } from '../util'
import { forOwn, isArray, isString } from 'lodash'
import { expr as qb , Expression} from 'squel'

//////////////////////////////////////////////////////////////////////////////////////////////////
// SQLFilter Interfaces

 /**FilterOptions
  * options for filtering.
  * available options:
  *        * prefix     : add a prefix to table filter.
  */
export interface FilterOptions {
	prefix?: string
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// SQLFilter

/**SQLFilter
 *
 * parse object filter structure into Arango Filter syntax.
 *
 *
 * @param filter        : Filter object defined by interface.
 * @param options       : options defined by FilterOptions Interface.
 */
export function SQLFilter (filter: Filter, options?: FilterOptions): Expression | null {
	const query = SQLFilterQueryBuilder(filter, options)
	if (query.toString().length > 0) {
		return query
	}
	return null
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// SQLFilter Helpers

/**SQLFilterQueryBuilder
 *
 * loop through given object and apply filter rules to arango.
 *
 *
 * @param filter        : Filter object defined by interface.
 * @param options       : options defined by FilterOptions Interface.
 */
function SQLFilterQueryBuilder (filter: Filter, options?: FilterOptions): Expression {
	let res: Expression = qb()

	if (!filter) {return res}

	// tslint:disable-next-line:no-any
	forOwn(filter, (value: any , key: string) => {
		if ((keywords.indexOf(key) !== -1) && isString(key) && isArray(value)) {
			if (key === '$and') {
				for (const v of value) {res = res.and(SQLFilterQueryBuilder(v, options))}
			}
			if (key === '$or') {
				for (const v of value) {res = res.or(SQLFilterQueryBuilder(v, options))}
			}
		}
		if ((keywords.indexOf(key) !== -1) && isString(key) && !isArray(value)) {
			if (key === '$not') {
				const strng = SQLFilterValue(key, SQLFilterQueryBuilder(value, options).toString())
				if (strng) {res.and(strng)}
			}
		}
		if (isString(key) && !isArray(value)) {
			const aq = SQLFilterValue(addTableAlias(key , options), value)
			if (aq) {res.and(aq)}
		}
	})
	return res
}

/**SQLFilterValue
 *
 * Apply filter rules to parse looped item in filter object.
 *
 *
 * @param key       : key to which filter rule belongs to.
 * @param value     : Filter item defining rule to parse.
 */
// tslint:disable-next-line:no-any
function SQLFilterValue (key: string , value: any): string | undefined {
	if (value.$eq) {
		return `${key} = '${value.$eq}'`
	}
	if (value.$neq) {
		return `${key} != '${value.$neq}'`
	}
	if (value.$lt) {
		return `${key} < ${value.$lt}`
	}
	if (value.$lte) {
		return `${key} <= ${value.$lte}`
	}
	if (value.$gt) {
		return `${key} > ${value.$gt}`
	}
	if (value.$gte) {
		return `${key} >= ${value.$gte}`
	}
	if (value.$in ) {
		const p = [] as string[]
		value.$in.forEach((e: string) => {
			p.push(`'${e}'`)
		})
		return `${key} IN [${p}]`
	}
	if (key === '$not' && isString(value)) {
		return `NOT (${value})`
	}
	if (isString(key) && isString(value)) {
		return `${key} = '${value}'`
	}
	return undefined
}

export default SQLFilter