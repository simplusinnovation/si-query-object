//////////////////////////////////////////////////////////////////////////////////////////////////
// SQLUpdate requirements
/////////////////////////////////////////////////////////////////////////////////////////////////
import { Update, UpdateKeywords as keywords } from '../../types'
import { addTableAlias } from '../util'
import { forOwn, isArray, isString, keys, values, isNumber, isObject } from 'lodash'

//////////////////////////////////////////////////////////////////////////////////////////////////
// SQLUpdate Interfaces

 /**UpdateOptions
  * options for updateing.
  * available options:
  *        * prefix     : add a prefix to table update.
  */
export interface UpdateOptions {
	prefix: string
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// SQLUpdate

/**SQLUpdate
 *
 * parse object update structure into SQL update syntax.
 *
 *
 * @param update            : Update object defined by interface.
 * @param options           : options defined by UpdateOptions Interface.
 */
export function SQLUpdate (update: Update, options: UpdateOptions): {toString(): string} | null {
	const query = SQLUpdateQueryBuilder(update, options)
	if (query) {
		return query
	}
	return null
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// SQLUpdate Helpers

/**SQLUpdateQueryBuilder
 *
 * loop through given object and apply update rules to SQL.
 *
 *
 * @param update            : Update object defined by interface.
 * @param options           : options defined by UpdateOptions Interface.
 */
function SQLUpdateQueryBuilder (update: Update, options: UpdateOptions): {toString(): string} | null {
	let res: string[] = []
	SQLUpdateValue.resetCols()
	if (!update) {return null}
	// tslint:disable-next-line:no-any
	forOwn(update, (value: any, key: string) => {
		res = SQLUpdateValue.addNewSet(value, key, options)
		// if ((keywords.indexOf(key) !== -1) && isString(key)) {
		// }
	})
	return res
}

/**SQLUpdateValue
 *
 * Apply update rules to parse looped item in update object.
 */
class SQLUpdateValue {
	/** list of cols with new values. */
	private static cols: string [] = []
	/** creates set of colmns ot include in update query. */
	private static newSet(): {toString(): string} {
		return {
			toString: (): string => {
				return SQLUpdateValue.cols.join(' , ')
			},
		}
	}
	/** reset list of columns in the current join list. */
	static resetCols(): void {
		this.cols = []
		return
	}
	// tslint:disable-next-line:no-any
	static assignSQL(value: any, keyword: string, alias: string): string {
		switch (keyword) {
			case '$inc':
				return `${alias} = (${alias} + ${values(value)[0]})`
			case '$dec':
				return `${alias} = (${alias} - ${values(value)[0]})`
			case '$set':
				const _v = values(value)[0]
				return isNumber(_v) ? `${alias} = ${Number(_v)}` : `${alias} = ${String(_v)}`
			default:
				return isNumber(value) ? `${alias} = ${Number(value)}`
					: (isObject(value) ? `${alias} = ${JSON.stringify(value)}`
					: (isArray(value) ? `${alias} = ${JSON.stringify(value)}`
					: `${alias} = ${String(value)}`))
		}
	}
	/**addNewSet
     *
     * Apply update rules to parse looped item in update object.
     * @param value         : Update item defining rule to parse.
     * @param keyword       : keyword defining type of operation required.
     * @param options       : options defined in UpdateOptions.
     */
	// tslint:disable-next-line:no-any
	static addNewSet(value: any[] | any , keyword: string  , options: UpdateOptions): any {
		if (isArray(value) && ((keywords.indexOf(keyword) !== -1) && isString(keyword))) {
			// tslint:disable-next-line:no-any
			value.forEach((v) => {
				SQLUpdateValue.cols.push(SQLUpdateValue.assignSQL(v, keyword, addTableAlias(keys(v)[0], options)))
			})
		} else if (isArray(value)) {
			// tslint:disable-next-line:no-any
			const store: any[] = []
			value.forEach((v) => {
				store.push(v)
			})
			SQLUpdateValue.cols.push(SQLUpdateValue.assignSQL(store, keyword, addTableAlias(keyword, options)))
		} else if ((keywords.indexOf(keyword) !== -1) && isString(keyword)) {
			SQLUpdateValue.cols.push(SQLUpdateValue.assignSQL(value, keyword, addTableAlias(keys(value)[0], options)))
		} else {
			SQLUpdateValue.cols.push(SQLUpdateValue.assignSQL(value, keyword, addTableAlias(keyword, options)))
		}

		return SQLUpdateValue.newSet()
	}
}

export default SQLUpdate