//////////////////////////////////////////////////////////////////////////////////////////////////
// General helper utilities

/**addTableAlias
 *
 * appends table prefix to column.
 * @param key           : column to append prefix.
 * @param options       : prefix to append to column.
 */
// tslint:disable-next-line:no-any
export function addTableAlias (key: string , options?: any): string {
	if (key.indexOf('.') === -1 && options && options.prefix) {
		return `${options.prefix}.${key}`
	}
	return key
}