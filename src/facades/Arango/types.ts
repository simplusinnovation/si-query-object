export interface ObjectLiteral {
	toAQL(): string;
	_value: object;
}
export interface BinaryOperation  {
	toAQL(): string;
	_operator: string;
}
export interface NAryOperation {
	toAQL(): string;
	_operator: string;
	// tslint:disable-next-line:no-any
	_values: any[];
}

export interface AQLfunctions {
	// tslint:disable-next-line:no-any
	and(...x: any[]): NAryOperation;
	// tslint:disable-next-line:no-any
	or(...x: any[]): NAryOperation;
	toAQL(): string;
}
export interface NumberLiteral {
	toAQL(): string;
	re: RegExp;
}
export interface BooleanLiteral {
	toAQL(): string;
	_value: boolean;
}
export interface StringLiteral {
	toAQL(): string;
}
export interface ListLiteral {
	toAQL(): string;
}