//////////////////////////////////////////////////////////////////////////////////////////////////
// ArangoUpdate requirements
/////////////////////////////////////////////////////////////////////////////////////////////////
import { forOwn, assign, values, keys, isArray, isNumber, isBoolean, isObject, isString } from 'lodash';
import { ObjectLiteral, NAryOperation, NumberLiteral, BooleanLiteral, StringLiteral, ListLiteral} from './types';
import { Update, keywords } from '../../types/Update';
import { addTableAlias } from '../util';
import qb = require('aqb');

//////////////////////////////////////////////////////////////////////////////////////////////////
// ArangoUpdate Interfaces

 /**UpdateOptions
  * options for updateing.
  * available options:
  *        * prefix     : add a prefix to table update.
  */
export interface UpdateOptions {
	prefix?: string
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// ArangoUpdate

/**ArangoUpdate
 *
 * parse object update structure into Arango update syntax.
 *
 *
 * @param update            : Update object defined by interface.
 * @param options           : options defined by UpdateOptions Interface.
 */
export function ArangoUpdate (update: Update, options?: UpdateOptions): ObjectLiteral | null {
	const query = ArangoUpdateQueryBuilder(update, options)
	if (query) {return query}
	return null
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// ArangoUpdate Helpers

/**ArangoUpdateQueryBuilder
 *
 * loop through given object and apply update rules to arango.
 *
 *
 * @param update            : Update object defined by interface.
 * @param options           : options defined by UpdateOptions Interface.
 */
function ArangoUpdateQueryBuilder(update: Update, options?: UpdateOptions): ObjectLiteral | null {
	if (!update) { return null }
	let cols = {} as object
	// tslint:disable-next-line:no-any
	forOwn(update, (value: any, key: string) => {
		cols = assign(cols, ArangoUpdateValue(value, key, options))
	})
	return qb.obj(cols)
}

/**ArangoUpdateValue
 *
 * Apply update rules to parse looped item in update object.
 *
 *
 * @param value         : Update item defining rule to parse.
 * @param keyword       : keyword defining type of operation required.
 * @param options       : options defined in UpdateOptions.
 */
// tslint:disable-next-line:no-any
function ArangoUpdateValue (value: any[] | any , keyword: string , options?: UpdateOptions):  {[x: string]: NAryOperation | NumberLiteral | BooleanLiteral | StringLiteral| ListLiteral} {
	// tslint:disable-next-line:no-any
	const cols = {} as {[x: string]: any | any[] }
	if (isArray(value) && ((keywords.indexOf(keyword) !== -1) && isString(keyword))) {
		// tslint:disable-next-line:no-any
		value.forEach((v) => {
			cols[keys(v)[0]] = assignAQB(v, keyword, addTableAlias(keys(v)[0], options))
		})
	} else if (isArray(value)) {
		// tslint:disable-next-line:no-any
		const store: any[] = []
		value.forEach((v) => {
			store.push(assignAQB(v, keyword, addTableAlias(keyword, options)))
		})
		cols[keyword] = qb.list(store)
	} else if ((keywords.indexOf(keyword) !== -1) && isString(keyword)) {
		cols[keys(value)[0]] = assignAQB(value, keyword, addTableAlias(keys(value)[0], options))
	} else {
		cols[keyword] = assignAQB(value, keyword, addTableAlias(keyword, options))
	}

	return cols as {[x: string]: NAryOperation | NumberLiteral | BooleanLiteral | StringLiteral | ListLiteral}
}

/**assignAQB
 *
 * return aqb version of object item.
 *
 *
 * @param value         : Update item defining rule to parse.
 * @param keyword       : keyword defining type of operation required.
 * @param alias       	: only used in $inc and $dec methods
 */
// tslint:disable-next-line:no-any
function assignAQB (value: any, keyword: string, alias: string): NAryOperation | NumberLiteral | BooleanLiteral | StringLiteral | ListLiteral {
	switch (keyword) {
		case '$inc':
			return  qb.ref(alias).add(qb.num(values(value)[0]))
		case '$dec':
			return qb.ref(alias).sub(qb.num(values(value)[0]))
		case '$set':
			const _vset = isObject(value) ? values(value)[0] : value
			// tslint:disable-next-line:no-any
			return isArray(value) ? qb.list(assignAQB(_vset, keyword, alias) as any)
						: (isBoolean(value) ? qb.bool(_vset)
						: (isNumber(value) ? qb.num(value)
						// : (isObject(value) ? qb.obj(qb(_vset))
						: qb.str(_vset)))
		default:
			// tslint:disable-next-line:no-any
			return isArray(value) ? qb.list(assignAQB(value, keyword, alias) as any)
						: (isBoolean(value) ? qb.bool(value)
						: (isNumber(value) ? qb.num(value)
						: (isObject(value) ? qb.obj(qb(value))
						: qb.str(value))))
	}
}
export default ArangoUpdate