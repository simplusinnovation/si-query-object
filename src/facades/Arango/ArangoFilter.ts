//////////////////////////////////////////////////////////////////////////////////////////////////
// ArangoFilter requirements
/////////////////////////////////////////////////////////////////////////////////////////////////
import { forOwn, isArray, isString, isNumber, isBoolean } from 'lodash';
import { AQLfunctions, BinaryOperation, NAryOperation} from './types';
import { Filter, FilterKeywords as keywords, BinaryOperations } from '../../types';
import { addTableAlias } from '../util';
import qb = require('aqb');

//////////////////////////////////////////////////////////////////////////////////////////////////
// ArangoFilter Interfaces

 /**FilterOptions
  * options for filtering.
  * available options:
  *        * prefix     : add a prefix to table filter.
  */
export interface FilterOptions {
	prefix?: string
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// ArangoFilter

/**ArangoFilter
 *
 * parse object filter structure into Arango Filter syntax.
 *
 *
 * @param filter        : Filter object defined by interface.
 * @param options       : options defined by FilterOptions Interface.
 */
export function ArangoFilter (filter: Filter, options?: FilterOptions):  AQLfunctions | BinaryOperation | NAryOperation | null {
	const query = ArangoFilterQueryBuilder(filter, options)
	if (query) {
		return query
	}
	return null
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// ArangoFilter Helpers

/**ArangoFilterQueryBuilder
 *
 * loop through given object and apply filter rules to arango.
 *
 *
 * @param filter        : Filter object defined by interface.
 * @param options       : options defined by FilterOptions Interface.
 */
function ArangoFilterQueryBuilder (filter: Filter, options?: FilterOptions): AQLfunctions | BinaryOperation | NAryOperation | null {
	let res  = qb({})

	if (!filter) {return null}

	// tslint:disable-next-line:no-any
	forOwn(filter, (value: any , key: string) => {
		if ((keywords.indexOf(key) !== -1) && isString(key) && isArray(value)) {
			if (key === '$and') {
				for (const v of value) {
					const temp = ArangoFilterQueryBuilder(v, options)
					// tslint:disable-next-line:no-any
					if (temp) {res = (!(res as any)._operator) ? (temp as any) : (res.and(temp) as any)}
				}
			}
			if (key === '$or') {
				for (const v of value) {
					const temp = ArangoFilterQueryBuilder(v, options)
					// tslint:disable-next-line:no-any
					if (temp) {res = (!(res as any)._operator) ? (temp as any) : (res.or(temp) as any)}
				}
			}
		}
		if ((keywords.indexOf(key) !== -1) && isString(key) && !isArray(value)) {
			if (key === '$not') {
				const temp = ArangoFilterQueryBuilder(value, options)
				// tslint:disable-next-line:no-any
				res = (!(res as any)._operator) ? qb.not(temp) as any : res = res.and(qb.not(temp)) as any
			}
		}
		if (isString(key) && !isArray(value)) {
			const aq = ArangoFilterValue(addTableAlias(key, options), value)
			// tslint:disable-next-line:no-any
			if (aq) {res = (!(res as any)._operator) ? aq as any : res.and(aq) as any}
		}
	})
	// tslint:disable-next-line:no-any
	return res as any
}

/**ArangoFilterValue
 *
 * Apply filter rules to parse looped item in filter object.
 *
 *
 * @param key       : key to which filter rule belongs to.
 * @param value     : Filter item defining rule to parse.
 */
function ArangoFilterValue (key: string , value: BinaryOperations): BinaryOperation | null {
	if (value.$eq) {
		return qb.ref(key).eq(isNumber(value.$eq) ? qb.num(value.$eq) : (isBoolean(value.$eq) ? qb.bool(value.$eq) : qb.str(value.$eq)))
	}
	if (value.$neq) {
		return qb.ref(key).neq(isNumber(value.$neq) ? qb.num(value.$neq) : (isBoolean(value.$neq) ? qb.bool(value.$neq) : qb.str(value.$neq)))
	}
	if (value.$lt) {
		return qb.ref(key).lt(qb(value.$lt))
	}
	if (value.$lte) {
		return qb.ref(key).lte(qb(value.$lte))
	}
	if (value.$gt) {
		return qb.ref(key).gt(qb(value.$gt))
	}
	if (value.$gte) {
		return qb.ref(key).gte(qb(value.$gte))
	}
	if (value.$in) {
		return qb.ref(key).in(value.$in.map((v: string) => `"${v}"`))
	}
	if (isString(value) || isNumber(value)) {
		return qb.ref(key).eq(isNumber(value) ? qb.num(value) : `"${value}"`)
	}
	return null
}

export default ArangoFilter